//  GET = retrieves || POST = create || PUT = update || DELETE = delete  
// npx kill-port 4000

let http = require("http");

http.createServer((request, response) => {
        
    if (request.url == "/profile" && request.method == "GET" ){
        response.writeHead(200,{"Content-Type":"text/plain"});
        response.end("Welcome to your proﬁle");
    };

    if (request.url == "/courses" && request.method == "GET" ){
        response.writeHead(200,{"Content-Type":"text/plain"});
        response.end("Here’s our courses available");    
    };

    if (request.url == "/" && request.method == "GET" ) {
        response.writeHead(200,{"Content-Type":"text/plain"});
        response.end("Welcome to Booking System");    
    };

    if (request.url == "/addCourse" && request.method == "POST" ){
        response.writeHead(200,{"Content-Type":"text/plain"});
        response.end("Add a course to our resources");    
    };

    if (request.url == "/updateCourse" && request.method == "PUT" ){
        response.writeHead(200,{"Content-Type":"text/plain"});
        response.end(" Update a course to our resources");    
    };

    if (request.url == "/archiveCourses" && request.method == "DELETE" ){
        response.writeHead(200,{"Content-Type":"text/plain"});
        response.end("Archive courses to our resources");    
    };
        
}).listen(4000);

// if port is already opened, use the npx kill-port <PortNumber> to close the port

console.log(`Server running at localhost: 4000`);